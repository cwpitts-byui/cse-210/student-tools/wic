import tempfile
from argparse import Namespace
from pathlib import Path

import pytest
from wic.__main__ import main, parse_args

data_dir: Path = Path(__file__).parent / "data"


@pytest.mark.parametrize(
    "url, is_file, expected_return",
    [
        ("git@gitlab.com:cwpitts-byui/cse-210/student-tools/wic.git", False, 0),
        ("git@gitlab.com:cwpitts-byui/cse-210/student-tools/wic", False, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic.git", False, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic", False, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wicc", False, 1),
        (
            "https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic/tree/master",
            False,
            1,
        ),
        ("git@gitlab.com:cwpitts-byui/cse-210/student-tools/wic.git", True, 0),
        ("git@gitlab.com:cwpitts-byui/cse-210/student-tools/wic", True, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic.git", True, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic", True, 0),
        ("https://gitlab.com/cwpitts-byui/cse-210/student-tools/wicc", True, 1),
        (
            "https://gitlab.com/cwpitts-byui/cse-210/student-tools/wic/tree/master",
            True,
            1,
        ),
    ],
)
def test_main(url, is_file, expected_return):
    if is_file:
        with tempfile.NamedTemporaryFile() as tmp_file:
            tmp_file_path = Path(tmp_file.name)
            with open(tmp_file_path, "w") as out_file:
                out_file.write(f"{url}\n")
            args: Namespace = Namespace(url=tmp_file_path, file=is_file)
            actual_return: int = main(args)
    else:
        args: Namespace = Namespace(url=url, file=is_file)
        actual_return: int = main(args)
    assert actual_return == expected_return
